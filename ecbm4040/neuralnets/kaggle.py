import tensorflow as tf
import numpy as np
import time
from ecbm4040.image_generator import ImageGenerator

class conv_layer(object):
    def __init__(self, inputs, size, filters, stride, decay, name, dropout = 0.7,rand_seed = 234, bn=False):
        """
        :param input_x: The input of the conv layer. Should be a 4D array like (batch_num, img_len, img_len, channel_num)
        :param in_channel: The 4-th demension (channel number) of input matrix. For example, in_channel=3 means the input contains 3 channels.
        :param out_channel: The 4-th demension (channel number) of output matrix. For example, out_channel=5 means the output contains 5 channels (feature maps).
        :param kernel_shape: the shape of the kernel. For example, kernal_shape = 3 means you have a 3*3 kernel.
        :param rand_seed: An integer that presents the random seed used to generate the initial parameter value.
        :param name: The name of the layer. It is used for naming only.
        """
        self.wd = 1e-2
        self.dropout = dropout
        self.sizes = []
        self.flops = []
        self.training = tf.placeholder_with_default(False, shape=[], name="training")
        channels = inputs.get_shape()[3]
        shape = [size, size, channels, filters]
        with tf.variable_scope('conv_layer_%s' % name) as scope:
            with tf.name_scope('conv_kernel'):
                self.weight = self._get_weights_var('conv_kernel_%s' % name,
                                            shape=shape,
                                            decay=decay)

            with tf.variable_scope('conv_bias'):
                b_shape = [filters]
                bias = tf.get_variable(name='conv_bias_%s' % name, shape=b_shape,
                                       initializer=tf.glorot_uniform_initializer(seed=rand_seed))
                self.bias = bias

            # strides [1, x_movement, y_movement, 1]
            conv = tf.nn.conv2d(inputs,
                                self.weight,
                                strides=[1,stride,stride,1],
                                padding='SAME')
            
            if bn:
                conv = tf.layers.batch_normalization(conv,
                                                     training=self.training)
            pre_activation = tf.nn.bias_add(conv, self.bias)

            outputs= tf.nn.relu(pre_activation, name=name)
            
            tf.summary.histogram('conv_layer/{}/kernel'.format(name), self.weight)
            tf.summary.histogram('conv_layer/{}/bias'.format(name), self.bias)

        
            self.cell_out = outputs

        

    def _get_weights_var(self, name, shape, decay=False):
        """Helper to create an initialized Variable with weight decay.
        The Variable is initialized using a normal distribution whose variance
        is provided by the xavier formula (ie inversely proportional to the number
        of inputs)
        Args:
            name: name of the tensor variable
            shape: the tensor shape
            decay: a boolean indicating if we apply decay to the tensor weights
            using a regularization loss
        Returns:
            Variable Tensor
        """
        # Declare an initializer for this variable
        initializer = tf.contrib.layers.xavier_initializer(uniform=False,dtype=tf.float32)
        # Declare variable (it is trainable by default)
        var = tf.get_variable(name=name,
                              shape=shape,
                              initializer=initializer,
                              dtype=tf.float32)
        if decay:
            # We apply a weight decay to this tensor var that is equal to the
            # model weight decay divided by the tensor size
            weight_decay = self.wd
            for x in shape:
                #print(x,weight_decay)
                weight_decay /= int(x)
                
            # Weight loss is L2 loss multiplied by weight decay
            weight_loss = tf.multiply(tf.nn.l2_loss(var),
                                      weight_decay,
                                      name='weight_loss')
            # Add weight loss for this variable to the global losses collection
            tf.add_to_collection('losses', weight_loss)

        return var
        
        
    def output(self):
        return self.cell_out


class max_pooling_layer(object):
    def __init__(self, inputs, size, stride, name):
        """
        :param input_x: The input of the pooling layer.
        :param k_size: The kernel size you want to behave pooling action.
        :param padding: The padding setting. Read documents of tf.nn.max_pool for more information.
        """
        with tf.variable_scope('max_pooling') as scope:
            outputs = tf.nn.max_pool(inputs,
                                     ksize=[1,size,size,1],
                                     strides=[1,stride,stride,1],
                                     padding='SAME',
                                     name=name)
            self.cell_out = outputs
        
    def output(self):
        return self.cell_out


'''class norm_layer(object):
    def __init__(self, inputs, name = "lrn"):
        """
        :param input_x: The input that needed for normalization.
        """
        depth_radius=4
        with tf.variable_scope(name) as scope:
            self.outputs = tf.nn.lrn(inputs,
                                depth_radius=depth_radius,
                                bias=1.0,
                                alpha=0.001/9.0,
                                beta=0.75,
                                name=scope.name)


    def output(self):
        return self.outputs'''


class fc_layer(object):
    def __init__(self, inputs, neurons, decay,name, relu=True, bn=False, wd = 1e-2):
        """
        :param input_x: The input of the FC layer. It should be a flatten vector.
        :param in_size: The length of input vector.
        :param out_size: The length of output vector.
        :param rand_seed: An integer that presents the random seed used to generate the initial parameter value.
        :param keep_prob: The probability of dropout. Default set by 1.0 (no drop-out applied)
        :param activation_function: The activation function for the output. Default set to None.
        :param name: The name of the layer. It is used for naming only.

        """
        self.wd = wd
        with tf.variable_scope('fc_layer_%s' % name) as scope:
            if len(inputs.get_shape().as_list()) > 2:
                # We need to reshape inputs:
                #   [ batch size , w, h, c ] -> [ batch size, w x h x c ]
                # Batch size is a dynamic value, but w, h and c are static and
                # can be used to specifiy the reshape operation
                dim = np.prod(inputs.get_shape().as_list()[1:])
                reshaped = tf.reshape(inputs, shape=[-1, dim], name='reshaped')
            else:
                # No need to reshape inputs
                reshaped = inputs
            dim = reshaped.get_shape().as_list()[1]
            
            with tf.name_scope('fc_kernel'):
                self.weight = self._get_weights_var('fc_kernel_%s' % name,
                                            shape=[dim,neurons],
                                            decay=decay)

            with tf.variable_scope('fc_bias'):
                bias = tf.get_variable('fc_bias_%s' % name,
                                    shape=[neurons],
                                    dtype=tf.float32,
                                    initializer=tf.constant_initializer(0.0))
                self.bias = bias

            x = tf.add(tf.matmul(reshaped, self.weight), self.bias)
            if bn:
                x = tf.layers.batch_normalization(x, training=self.training)
            if relu:
                outputs = tf.nn.relu(x)
            else:
                outputs = x
                
            self.cell_out = outputs

            tf.summary.histogram('fc_layer/{}/kernel'.format(name), self.weight)
            tf.summary.histogram('fc_layer/{}/bias'.format(name), self.bias)
            
    def _get_weights_var(self, name, shape, decay=False):
        """Helper to create an initialized Variable with weight decay.
        The Variable is initialized using a normal distribution whose variance
        is provided by the xavier formula (ie inversely proportional to the number
        of inputs)
        Args:
            name: name of the tensor variable
            shape: the tensor shape
            decay: a boolean indicating if we apply decay to the tensor weights
            using a regularization loss
        Returns:
            Variable Tensor
        """
        # Declare an initializer for this variable
        initializer = tf.contrib.layers.xavier_initializer(uniform=False,dtype=tf.float32)
        # Declare variable (it is trainable by default)
        var = tf.get_variable(name=name,
                              shape=shape,
                              initializer=initializer,
                              dtype=tf.float32)
        if decay:
            # We apply a weight decay to this tensor var that is equal to the
            # model weight decay divided by the tensor size
            weight_decay = self.wd
            for x in shape:
                weight_decay /= x
            # Weight loss is L2 loss multiplied by weight decay
            weight_loss = tf.multiply(tf.nn.l2_loss(var),
                                      weight_decay,
                                      name='weight_loss')
            # Add weight loss for this variable to the global losses collection
            tf.add_to_collection('losses', weight_loss)

        return var
        
    def output(self):
        return self.cell_out
    
    
    
    
    
def my_LeNet(input_x, input_y, keep_prob = 0.7):
    """
        LeNet is an early and famous CNN architecture for image classfication task.
        It is proposed by Yann LeCun. Here we use its architecture as the startpoint
        for your CNN practice. Its architecture is as follow.

        input >> Conv2DLayer >> Conv2DLayer >> flatten >>
        DenseLayer >> AffineLayer >> softmax loss >> output

        Or

        input >> [conv2d-maxpooling] >> [conv2d-maxpooling] >> flatten >>
        DenseLayer >> AffineLayer >> softmax loss >> output

        http://deeplearning.net/tutorial/lenet.html

    """

    # conv layer
    conv_layer_1 = conv_layer(inputs = input_x,
                                size=5,
                                filters=8,
                                stride=1,
                                decay=True,
                                name='conv1', bn = True)
    
    dropout_1 = tf.nn.dropout(conv_layer_1.output(), keep_prob)

    pooling_layer_1 = max_pooling_layer(inputs=dropout_1,
                                        size=3,
                                stride=2,
                                name='pool1')
                                        
    #norm_layer_1 = norm_layer(inputs = pooling_layer_1.output())
    #print (pooling_layer_0.output().shape,len(pooling_layer_0.output().shape))
    
    conv_layer_2 = conv_layer(inputs = pooling_layer_1.output(),
                                size=3,
                                filters=16,
                                stride=1,
                                decay=True,
                                name='conv2', bn = True)
    dropout_2 = tf.nn.dropout(conv_layer_2.output(), keep_prob)
    
    #norm_layer_2 = self.lrn_layer(conv_layer_2.output(), name='norm2')
    
    pooling_layer_2 = max_pooling_layer(inputs = dropout_2,
                                size=3,
                                stride=2,
                                name='pool2')
    
    conv_layer_3 = conv_layer(inputs = pooling_layer_2.output(),
                                size=3,
                                filters=32,
                                stride=1,
                                decay=True,
                                name='conv3', bn = True)
    dropout_3 = tf.nn.dropout(conv_layer_3.output(), keep_prob)
    
    #norm_layer_2 = self.lrn_layer(conv_layer_2.output(), name='norm2')
    
    pooling_layer_3 = max_pooling_layer(inputs = dropout_3,
                                size=3,
                                stride=2,
                                name='pool3')
    
    conv_layer_4 = conv_layer(inputs = pooling_layer_3.output(),
                                size=3,
                                filters=128,
                                stride=1,
                                decay=True,
                                name='conv4', bn = True)

    
    #norm_layer_2 = self.lrn_layer(conv_layer_2.output(), name='norm2')
    
    pooling_layer_4 = max_pooling_layer(inputs = conv_layer_4.output(),
                                size=3,
                                stride=2,
                                name='pool4')
    
    conv_layer_5 = conv_layer(inputs = pooling_layer_4.output(),
                                size=3,
                                filters=192,
                                stride=1,
                                decay=True,
                                name='conv5', bn = True)

    
    #norm_layer_2 = self.lrn_layer(conv_layer_2.output(), name='norm2')
    
    pooling_layer_5 = max_pooling_layer(inputs = conv_layer_5.output(),
                                size=3,
                                stride=2,
                                name='pool5')
    
    
    # flatten
    pool_shape = pooling_layer_5.output().get_shape()
    img_vector_length = pool_shape[1].value * pool_shape[2].value * pool_shape[3].value
    flatten = tf.reshape(pooling_layer_5.output(), shape=[-1, img_vector_length])

    # fc layer
    fc_layer_1 = fc_layer(inputs = pooling_layer_5.output(),
                            neurons=384,
                            decay=True,
                            name='fc1')

    fc_layer_2 = fc_layer(inputs=fc_layer_1.output(),
                          neurons=192,
                            decay=True,
                            name='fc2')
    
    fc_layer_3 = fc_layer(inputs=fc_layer_2.output(),
                          neurons=10,
                            decay=False,
                            relu=False,
                            name='fc3')

 
    # loss
    with tf.name_scope("loss"):
        #print(fc_layer_3.output().shape)
        
        labels = tf.one_hot(input_y, 10)
        #print(labels.shape)
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=labels,
                                                                       logits=fc_layer_3.output(),name='cross_entropy_per_example')
        
        cross_entropy_loss = tf.reduce_mean(cross_entropy,
                                            name='cross_entropy_loss')
        # We use a global collection to track losses
        tf.add_to_collection('losses', cross_entropy_loss)

        # The total loss is the sum of all losses, including the cross entropy
        # loss and all of the weight losses (see variables declarations)
        total_loss = tf.add_n(tf.get_collection('losses'), name='total_loss')
        tf.summary.scalar('LeNet_loss', total_loss)


    return fc_layer_3.output(), total_loss


def cross_entropy(output, input_y):
    with tf.name_scope('cross_entropy'):
        label = tf.one_hot(input_y, 10)
        ce = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=label, logits=output))

    return ce


def train_step(loss, learning_rate=1e-3):
    with tf.name_scope('train_step'):
        step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)

    return step


def evaluate(output, input_y):
    with tf.name_scope('evaluate'):
        pred = tf.argmax(output, axis=1)
        error_num = tf.count_nonzero(pred - input_y, name='error_num')
        tf.summary.scalar('LeNet_error_num', error_num)
    return error_num

####################################
#        End of your code          #
####################################

##########################################
# TODO: Build your own training function #
##########################################


def my_training(X_train, y_train, X_val, y_val, 
             seed=235,
             learning_rate=1e-2,
             epoch=20,
             batch_size=245,
             verbose=False,
             pre_trained_model=None):
    print("Building my LeNet. Parameters: ")
    print("learning_rate={}".format(learning_rate))

    # define the variables and parameter needed during training
    with tf.name_scope('inputs'):
        xs = tf.placeholder(shape=[None, 128, 128, 3], dtype=tf.float32)
        ys = tf.placeholder(shape=[None, ], dtype=tf.int64)

    output, loss = my_LeNet(xs, ys)

    iters = int(X_train.shape[0] / batch_size)
    print('number of batches for training: {}'.format(iters))

    step = train_step(loss)
    eve = evaluate(output, ys)

    iter_total = 0
    best_acc = 0
    cur_model_name = 'lenet_{}'.format(int(time.time()))

    with tf.Session() as sess:
        merge = tf.summary.merge_all()

        writer = tf.summary.FileWriter("log/{}".format(cur_model_name), sess.graph)
        saver = tf.train.Saver()
        sess.run(tf.global_variables_initializer())

        # try to restore the pre_trained
        if pre_trained_model is not None:
            try:
                print("Load the model from: {}".format(pre_trained_model))
                saver.restore(sess, 'model/{}'.format(pre_trained_model))
            except Exception:
                print("Load model Failed!")
                pass
        ig = ImageGenerator(X_train,y_train)
  
        for epc in range(epoch):
            print("epoch {} ".format(epc + 1))

            for training_batch_x, training_batch_y in ig.next_batch_gen(batch_size):
                
                iter_total += 1
                _, cur_loss = sess.run([step, loss], feed_dict={xs: training_batch_x, ys: training_batch_y})

                if iter_total % 100 == 0:
                    # do validation
                    valid_eve, merge_result = sess.run([eve, merge], feed_dict={xs: X_val, ys: y_val})
                    valid_acc = 100 - valid_eve * 100 / y_val.shape[0]
                    if verbose:
                        print('{}/{} loss: {} validation accuracy : {}%'.format(
                            batch_size * (itr + 1),
                            X_train.shape[0],
                            cur_loss,
                            valid_acc))

                    # save the merge result summary
                    writer.add_summary(merge_result, iter_total)

                    # when achieve the best validation accuracy, we store the model paramters
                    if valid_acc > best_acc:
                        print('Best validation accuracy! iteration:{} accuracy: {}%'.format(iter_total, valid_acc))
                        best_acc = valid_acc
                        saver.save(sess, 'model/{}'.format(cur_model_name))

                    if iter_total > 1000:
                        break
                if best_acc > 80:
                    break

    print("Traning ends. The best valid accuracy is {}. Model named {}.".format(best_acc, cur_model_name))